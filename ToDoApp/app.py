import os
from flask import Flask, render_template, request, redirect, url_for, jsonify, abort
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate


app = Flask(__name__)
app.config["SQLALCHEMY_DATABASE_URI"] = os.getenv("PI_POSTGRES_URI")
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
db = SQLAlchemy(app)
migrate = Migrate(app=app, db=db)


class Person(db.Model):
    __tablename__ = "people"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(), nullable=False)

    def __repr__(self):
        return self.__str__()

    def __str__(self):
        return f"ID: {self.id}, Name: {self.name}"


class ToDo(db.Model):
    """A todo item.

    :param description: string
    """

    __tablename__ = "todos"
    id = db.Column(db.Integer, primary_key=True)
    description = db.Column(db.String(), nullable=False)
    completed = db.Column(db.Boolean, nullable=False, default=False)

    def __repr__(self):
        return self.__str__()

    def __str__(self):
        return f"Id: {self.id}: {self.description}"


with app.app_context():
    db.create_all()


@app.route("/create", methods=["POST"])
def create_todo():
    error = False
    body = {}
    try:
        description = request.get_json()["description"]
        todo = ToDo(description=description, completed=False)
        db.session.add(todo)
        db.session.commit()
        body["id"] = todo.id
        body["completed"] = todo.completed
        body["description"] = todo.description
    except Exception as e:
        error = True
        db.session.rollback()
        print(e)
    finally:
        db.session.close()
    if error:
        abort(400)
    else:
        return jsonify(body)


@app.route("/completed", methods=["PATCH"])
def set_completed_todo():
    body = {}
    error = False
    try:
        completed = request.get_json()["completed"]
        todo_id = int(request.get_json()["todo_id"].strip())
        print("completed", completed)
        todo = db.session.execute(db.select(ToDo).filter_by(id=todo_id)).scalar_one()
        todo.completed = completed
        db.session.commit()
        body['completed'] = completed
    except Exception as e:
        error = True
        db.session.rollback()
        app.logger.error(e)
    finally:
        db.session.close()

    if error:
        return abort(400)
    else:
        return jsonify(body)


@app.route("/delete", methods=["DELETE"])
def delete_todo():
    body = {}
    error = False
    try:
        to_delete = request.get_json()["to_delete"]
        to_delete = int(to_delete.strip())
        todo = db.session.execute(db.select(ToDo).filter_by(id=to_delete)).scalar_one()
        db.session.delete(todo)        
        db.session.commit()
        body["deleted"] = to_delete
    except Exception as e:
        error = True
        db.session.rollback()
        app.logger.error(e)        
    finally:
        db.session.close()

    if error:
        abort(404)
    else:
        # return redirect(url_for("index"))
        return jsonify(body)


@app.route("/")
def index():
    return render_template("index_async.html", todos=ToDo.query.order_by("id").all())


if __name__ == "__main__":
    app.run("localhost", debug=True)
