# PythonBackendPortfolio

![](_docs/images/Python%20classes%20DB%20tables%20mapping.png)
![](_docs/images/object_lifecycle_SQLAlchemy.png)
![](_docs/images/sqlalchemy-layers.png)
![](_docs/images/SQLAlchemy%20layers%20and%20responsibilities.png)
![](_docs/images/MVC_pattern.png)

### Overall Steps to Set Up & Run Migrations

1. Bootstrap database migrate commands: link to the Flask app models and database, link to command-line scripts for running migrations, set up folders to store migrations (as versions of the database)
2. Run initial migration to create tables for SQLAlchemy models, recording the initial schema: ala git init && first git commit. Replaces use of db.create_all()
3. Migrate on changes to our data models
   - Make changes to the SQLAlchemy models
   - Allow Flask-Migrate to auto-generate a migration script based on the changes
   - Fine-tune the migration scripts
   - Run the migration, aka “upgrade” the database schema by a “version”

It’s always helpful to read the docs!

https://alembic.sqlalchemy.org/en/latest/
    The main documentations site for Alembic with complete references for everything
https://flask-migrate.readthedocs.io/en/latest/
    The main documentations site for Flask-Migrate with complete references for everything


## AJAX Requests - Asynchronous
### Fetch - modern way

```js
// Syntax
fetch('<url-route>', '<object of request parameters>');

// Example usage
fetch('/endpoint/create', {
    method: 'POST',
    body: JSON.stringify(
        {
            'description': 'Get Butter and Cheese'
        })
    headers: {
        'Content-Type': 'appplication/json'
    }
}
);
```


### Old method - XMLHttpRequest
*Don't Use for new development*
```js
var xhttp = new XMLHttpRequest();
description = document.getElementById("description").value;
xhttp.open("GET", "/todos/create?description=" + description);
xhttp.send();

xhttp.onreadystatechange = function() {
    if (this.readyState === 4 && this.status === 200) { 
      // on successful response
      console.log(xhttp.responseText);
    }
};
```